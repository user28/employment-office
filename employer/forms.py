from django import forms

from employer.models import Employer


class EmployerProfileMainForm(forms.Form):
    company_picture = forms.ImageField(required=False)
    company_name = forms.CharField(max_length=50, required=True,
                                   widget=forms.TextInput(attrs={'placeholder': 'Tera Planer'}))
    is_in_search = forms.ChoiceField(choices=[('0', 'Разрешить'), ('1', 'Запретить')],
                                     widget=forms.Select(attrs={'class': 'chosen'}), required=False)
    since = forms.DateField(required=False)
    team_size = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': '123'}))
    description = forms.CharField(widget=forms.Textarea, required=False)

    def save(self, user):
        form_data = self.cleaned_data
        employer, created = Employer.objects.update_or_create(user=user,
                                                              defaults={'company_name': form_data['company_name'],
                                                                        'is_in_search': form_data['is_in_search'],
                                                                        'since': form_data['since'],
                                                                        'team_size': form_data['team_size'],
                                                                        'description': form_data['description']})

        return employer


class EmployerProfileContactForm(forms.Form):
    phone = forms.CharField(max_length=50, required=True,
                            widget=forms.TextInput(attrs={'placeholder': '+7 999 99 99'}))
    email = forms.CharField(max_length=50, required=True,
                            widget=forms.TextInput(attrs={'placeholder': 'example@ex.com'}))
    site = forms.CharField(max_length=50, required=True,
                           widget=forms.TextInput(attrs={'placeholder': 'www.example.com'}))
    city = forms.CharField(max_length=50, required=True,
                           widget=forms.TextInput(attrs={'placeholder': 'Киров'}))
    street = forms.CharField(max_length=50, required=True,
                             widget=forms.TextInput(attrs={'placeholder': 'Московская'}))
    house = forms.CharField(max_length=50, required=True,
                            widget=forms.TextInput(attrs={'placeholder': '20'}))
    body = forms.CharField(max_length=50, required=True,
                           widget=forms.TextInput(attrs={'placeholder': '1'}))

    def save(self, user):
        form_data = self.cleaned_data
        employer, created = Employer.objects.update_or_create(user=user, defaults={'phone': form_data['phone'],
                                                                                   'email': form_data['email'],
                                                                                   'site': form_data['site'],
                                                                                   'city': form_data['city'],
                                                                                   'street': form_data['street'],
                                                                                   'house': form_data['house'],
                                                                                   'body': form_data['body']})
        return employer


class EmployerNewJobDescription(forms.Form):
    name = forms.CharField(max_length=50, required=True,
                           widget=forms.TextInput(attrs={'placeholder': 'Дизайнер'}))
    description = forms.CharField(widget=forms.Textarea, required=True)

