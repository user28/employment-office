from django.db import models
from django.utils import timezone

from dictionary.models import User


class Employer(models.Model):
    class Meta:
        verbose_name = 'Работодатель'
        verbose_name_plural = 'Работодатели'

    user = models.OneToOneField(User, on_delete=models.PROTECT)

    company_name = models.CharField(max_length=50, blank=False, null=False)
    company_picture = models.ImageField(default='')
    is_in_search = models.BooleanField(default=True)
    since = models.DateField(default=timezone.now())
    team_size = models.CharField(max_length=50, default='')
    description = models.TextField(null=False, default='')
    phone = models.CharField(max_length=50, default='')
    email = models.CharField(max_length=50, default='')
    city = models.CharField(max_length=50, default='')
    street = models.CharField(max_length=50, default='')
    house = models.CharField(max_length=50, default='')
    site = models.CharField(max_length=50, default='')
    body = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.company_name

    def to_json(self):
        return {
            'company_name': self.company_name,
            'company_picture': self.company_picture,
            'is_in_search': self.is_in_search,
            'since': self.since,
            'team_size': self.team_size,
            'description': self.description,
            'phone': self.phone,
            'email': self.email,
            'city': self.city,
            'street': self.street,
            'house': self.house,
            'site': self.site,
            'body': self.body,
        }
