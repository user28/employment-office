from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from dictionary.models import Category
from employer.forms import EmployerProfileContactForm, EmployerProfileMainForm
from employer.models import Employer


def employer_list(request):
    categories = Category.objects.all()[:8]

    params = {
        'categories': categories,
    }
    return render(request, 'employer_list.html', params)


def employer_single(request):
    return render(request, 'employer_single.html')


@login_required(login_url='/login/')
def profile(request):
    user = request.user
    data = {}

    employer = Employer.objects.filter(user=user)
    if employer.exists():
        data = employer.get().to_json()

    params = {
        'main_form': EmployerProfileMainForm(data),
        'contact_form': EmployerProfileContactForm(data)
    }

    return render(request, 'employer_profile.html', params)


@login_required(login_url='/login/')
def jobs(request):
    return render(request, 'employer_jobs.html')


@login_required(login_url='/login/')
def resume(request):
    return render(request, 'employer_resumes.html')


def profile_contact(request):
    user = request.user

    if request.method == 'POST':
        form = EmployerProfileContactForm(request.POST)
        if form.is_valid():
            form.save(user)
            return profile(request)

    return redirect('index')


def profile_main(request):
    user = request.user

    if request.method == 'POST':
        form = EmployerProfileMainForm(request.POST)
        if form.is_valid():
            form.save(user)
            return profile(request)

    return redirect('index')
