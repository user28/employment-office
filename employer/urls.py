from django.urls import path
from . import views

urlpatterns = [
    path('', views.employer_list, name='employer_list'),
    path('single/', views.employer_single, name='employer_single'),
    path('profile/', views.profile, name='employer_profile'),
    path('profile/edit-main', views.profile_main, name='employer_profile_main'),
    path('profile/edit-contact', views.profile_contact, name='employer_profile_contact'),
    path('jobs/', views.jobs, name='employer_jobs'),
    path('resume/', views.resume, name='employer_resume'),
]
