from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from candidate.forms import CandidateEducationForm
from candidate.models import Candidate, CandidateEducation
from dictionary.models import Category, Experience


def candidate_list(request):
    categories = Category.objects.all()[:8]
    experience = Experience.objects.all()

    params = {
        'categories': categories,
        'experiences': experience,
    }

    return render(request, 'candidate_list.html', params)


def candidate_single(request):
    return render(request, 'candidate_single.html')


@login_required(login_url='/login/')
def candidate_profile(request):
    candidate = Candidate.objects.get(candidate_user=request.user)

    educations = ''
    educations = CandidateEducation.objects.filter(candidate_education_candidate=candidate)

    if educations.exists():
        educations = educations.all()

    params = {
        'educations': educations
    }

    return render(request, 'candidate_profile.html', params)


@login_required(login_url='/login/')
def candidate_resume(request):
    return render(request, 'candidate_resume.html')


@login_required(login_url='/login/')
def candidate_resume_new(request):
    return render(request, 'candidate_resume_new.html')


def candidate_education_new(request):
    params = {
        'form': CandidateEducationForm()
    }

    candidate = Candidate.objects.filter(candidate_user=request.user)

    if candidate.exists():
        candidate = candidate.get()

    if request.method == 'POST':
        form = CandidateEducationForm(request.POST)
        if form.is_valid():
            form.save(candidate)
            return redirect('candidate_profile')

    return render(request, 'canidate_education.html', params)


def candidate_education_delete(request, id):
    education = CandidateEducation.objects.get(id=id)
    education.delete()
    return redirect('candidate_profile')


def candidate_education_edit(request, id):
    candidate = Candidate.objects.filter(candidate_user=request.user)

    data = {}

    education = CandidateEducation.objects.get(id=id)

    if candidate.exists():
        data = education.to_json()

    params = {
        'form': CandidateEducationForm(data)
    }

    if request.method == 'POST':
        form = CandidateEducationForm(request.POST)
        if form.is_valid():
            form.save(candidate.get())
            return redirect('candidate_profile')

    return render(request, 'canidate_education.html', params)
