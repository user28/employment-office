from django.db import models

from dictionary.models import Category, User


class Candidate(models.Model):
    candidate_user = models.OneToOneField(User, on_delete=models.CASCADE)
    candidate_post = models.CharField(max_length=50, blank=True)
    candidate_is_in_search = models.BooleanField(default=True)
    candidate_min_salary = models.IntegerField()
    candidate_experience = models.CharField(max_length=50, blank=True)
    candidate_age = models.CharField(max_length=50, blank=True)
    candidate_current_salary_min = models.IntegerField()
    candidate_current_salary_max = models.IntegerField()
    candidate_offered_salary_min = models.IntegerField()
    candidate_offered_salary_max = models.IntegerField()
    candidate_education_level = models.CharField(max_length=50, blank=True)
    candidate_description = models.TextField(blank=True)
    candidate_phone = models.CharField(max_length=50)
    candidate_email = models.CharField(max_length=50)
    candidate_country = models.CharField(max_length=50)
    candidate_city = models.CharField(max_length=50)
    candidate_street = models.CharField(max_length=50)
    candidate_house = models.CharField(max_length=50)
    candidate_flat = models.CharField(max_length=50)
    candidate_body = models.CharField(max_length=50)

    def __str__(self):
        return self.candidate_user.get_full_name()


class CandidateEducation(models.Model):
    candidate_education_candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    candidate_education_institution = models.CharField(max_length=50, null=False)
    candidate_education_institution_title = models.CharField(max_length=255, null=False)
    candidate_education_since = models.CharField(max_length=255)
    candidate_education_to = models.CharField(max_length=255)
    candidate_education_education_level = models.CharField(max_length=255, blank=True)
    candidate_education_description = models.TextField(blank=True)

    def to_json(self):
        return {
            'education_level': self.candidate_education_education_level,
            'education_special' : self.candidate_education_institution,
            'education_since' : self.candidate_education_since,
            'education_to': self.candidate_education_to,
            'education_institute_title': self.candidate_education_institution_title,
            'education_description': self.candidate_education_description
        }


class CandidateWorkExperience(models.Model):
    candidate_work_experience_company = models.CharField(max_length=50, null=False)
    candidate_work_experience_position = models.CharField(max_length=50, null=False)
    candidate_work_experience_year_from = models.PositiveSmallIntegerField(null=False)
    candidate_work_experience_year_to = models.PositiveSmallIntegerField(null=False)
    candidate_work_experience_description = models.TextField(blank=True)


class CandidateSkills(models.Model):
    candidate_skills_title = models.CharField(max_length=50, null=False)
    candidate_skills_percentage = models.CharField(max_length=4, null=False)


class Resume(models.Model):
    class Meta:
        verbose_name = 'Резюме'
        verbose_name_plural = 'Резюме'

    resume_candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    resume_education = models.ForeignKey(CandidateEducation, on_delete=models.CASCADE)
    resume_work_experience = models.ForeignKey(CandidateWorkExperience, on_delete=models.CASCADE)
    resume_candidate_skills = models.ForeignKey(CandidateSkills, on_delete=models.CASCADE)
    resume_description = models.TextField(blank=True)
