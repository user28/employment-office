from django import forms

from candidate.models import CandidateEducation


class CandidateMainForm(forms.Form):
    fio = forms.CharField(max_length=255)
    post = forms.CharField(max_length=255)
    is_in_search = forms.ChoiceField(choices=[('0', 'Разрешить'), ('1', 'Запретить')],
                                     widget=forms.Select(attrs={'class': 'chosen'}), required=False)
    min_salary = forms.CharField(max_length=255)
    experience = forms.CharField(max_length=255)


class CandidateEducationForm(forms.Form):
    education_level = forms.CharField(max_length=255)
    education_special = forms.CharField(max_length=255)
    education_since = forms.CharField(max_length=4)
    education_to = forms.CharField(max_length=4)
    education_institute_title = forms.CharField(max_length=255)
    education_description = forms.CharField(widget=forms.Textarea)

    def save(self, candidate):
        form_data = self.cleaned_data
        education, created = CandidateEducation.objects. \
            update_or_create(candidate_education_candidate=candidate,
                             candidate_education_institution=form_data['education_special'],
                             defaults={'candidate_education_institution_title': form_data['education_institute_title'],
                                       'candidate_education_since': form_data['education_since'],
                                       'candidate_education_to': form_data['education_to'],
                                       'candidate_education_education_level': form_data['education_level'],
                                       'candidate_education_description': form_data['education_description']})
        return education
