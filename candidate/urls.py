from django.urls import path
from . import views

urlpatterns = [
    path('', views.candidate_list, name='candidate_list'),
    path('single/', views.candidate_single, name='candidate_single'),
    path('profile/', views.candidate_profile, name='candidate_profile'),
    path('profile/education/new/', views.candidate_education_new, name='candidate_education_new'),
    path('profile/education/<int:id>/edit/', views.candidate_education_edit, name='candidate_education_edit'),
    path('profile/education/<int:id>/delete/', views.candidate_education_delete, name='candidate_education_delete'),
    path('resume/', views.candidate_resume, name='candidate_resume'),
    path('resume/new/', views.candidate_resume_new, name='candidate_resume_new'),
]
