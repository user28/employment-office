from django import template

from dictionary.forms import LoginForm, RegistrationForm

register = template.Library()


@register.inclusion_tag('fragments/login_fields.html')
def login_fields():
    form = LoginForm()
    return {'login_form': form}


@register.inclusion_tag('fragments/registration_fields.html')
def register_fields():
    form = RegistrationForm()
    return {'reg_form': form}
