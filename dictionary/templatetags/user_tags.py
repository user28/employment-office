from django import template

from employer.models import Employer

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()


@register.filter(name='credentials')
def get_credentials(user):
    if user.groups.filter(name='Employer').exists():
        return Employer.objects.get(user=user).company_name if Employer.objects.filter(
            user=user).exists() else 'undefined'
    else:
        return '{} {}'.format(user.first_name, user.last_name.upper())
