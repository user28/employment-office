from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    email = models.EmailField(_('email address'), blank=False, null=False, unique=True)

    def is_candidate(self):
        self.groups.filter(name='Candidate').exists()

    def __str__(self):
        return '{} | {}'.format(self.get_full_name(), self.email)


class Vacancy(models.Model):
    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'

    vacancy_name = models.CharField(max_length=50, null=False)
    vacancy_position = models.CharField(max_length=50, null=False)
    vacancy_date_created = models.DateTimeField(auto_now_add=True)
    vacancy_status = models.PositiveSmallIntegerField(null=False)
    vacancy_work_schedule = models.CharField(max_length=50, null=False)
    vacancy_salary = models.IntegerField(null=False)
    vacancy_description = models.TextField()
    vacancy_employer = models.ForeignKey('dictionary.User', on_delete=models.PROTECT, related_name='Работодатель')

    def __str__(self):
        return self.vacancy_name


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'Категории'
        verbose_name = 'Категория'

    category_title = models.CharField(max_length=50, null=False, verbose_name='Направление деятельности')
    category_icon = models.CharField(max_length=50, null=False, verbose_name='Название картинки в шрифтах')
    vacancy_categories = models.ManyToManyField('dictionary.Vacancy', blank=True)
    employer_categories = models.ManyToManyField('employer.Employer', blank=True)

    def get_vacancies_count(self):
        return Category.objects.get(category_title=self.category_title).vacancy_categories.count()

    def __str__(self):
        return self.category_title


class Experience(models.Model):
    class Meta:
        verbose_name_plural = 'Опыт работ'
        verbose_name = 'Опыт работы'

    experience_title = models.CharField(max_length=50, null=False, verbose_name='Опыт работы (лет)')
    experience_suffix = models.CharField(max_length=50, blank=True, verbose_name='Как-то назвать надо')

    def __str__(self):
        return self.experience_title


class Age(models.Model):
    class Meta:
        verbose_name = 'Возраст'
        verbose_name_plural = 'Возраст'

    age_title = models.CharField(max_length=50, null=False)

    def __str__(self):
        return self.age_title


class Education(models.Model):
    class Meta:
        verbose_name_plural = 'Образование'
        verbose_name = 'Образование'

    education_level = models.CharField(max_length=50, null=False, verbose_name='Уровень образования')

    def __str__(self):
        return self.education_level
