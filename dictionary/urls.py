from django.urls import path

from dictionary import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('registration/', views.register, name='register'),
    path('logout/', views.logout, name='logout'),
    path('about/', views.about, name='about'),
    path('job/', views.job_list, name='job_list'),
    path('job/single/', views.job_single, name='job_single'),
    path('job/new/', views.job_new, name='job_new'),
    path('change_password/', views.change_password, name='change_password'),
]
