from enum import Enum


class Education(Enum):
    none = 0
    middle_not_complete = 1
    middle_complete = 2
    higher_not_complete = 3
    higher_complete = 4


class VacancyStatus(Enum):
    close = 0
    active = 1
