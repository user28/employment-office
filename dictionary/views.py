import django
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from dictionary.forms import RegistrationForm, LoginForm
from dictionary.models import User, Category


def handler404(request, *args, **argv):
    response = render(request, '404.html')
    response.status_code = 404
    return response


def index(request):
    categories = Category.objects.all()[:8]

    params = {
        'categories': categories,
    }

    if request.user.is_anonymous:
        registration_form = RegistrationForm()
        login_form = LoginForm()

        forms = {
            'reg_form': registration_form,
            'login_form': login_form
        }

        params.update(forms)

    return render(request, 'index.html', params)


def job_single(request):
    return render(request, 'job_single.html')


def job_list(request):
    categories = Category.objects.all()[:8]

    params = {
        'categories': categories,
    }

    return render(request, 'job_list.html', params)


@login_required(login_url='/login/')
def job_new(request):
    return render(request, 'job_new.html')


def about(request):
    return render(request, 'about.html')


@login_required(login_url='/login/')
def change_password(request):
    user = request.user

    if request.method == 'POST' and user.check_password(request.POST.get('old-password')):
        user.set_password(request.POST.get('new-password'))
        return render(request, 'change_password_success.html')

    return render(request, 'change_password.html')


def register(request):
    form = RegistrationForm()

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.register_user()
            if user is not None:
                form_data = form.cleaned_data
                user = authenticate(username=user.username, password=form_data['password'])
                django.contrib.auth.login(request, user)
                if user.groups.filter(name='Employer').exists():
                    return redirect('employer_profile')
                else:
                    return redirect('candidate_profile')

    return render(request, 'register.html', {
        'reg_form': form
    })


def login(request):
    next_url = request.GET.get('next')

    form = LoginForm()

    if request.method == 'POST':
        form = LoginForm(request.POST)
        next_url = request.POST.get('next', None)
        if form.is_valid():
            user = authenticate(username=User.objects.get(email=request.POST.get('email')).username,
                                password=request.POST.get('password'))
            if user is not None:
                django.contrib.auth.login(request, user)
                return redirect('index' if next_url is not None else next_url)
            else:
                return render(request, 'login.html', {
                    'login_form': form
                })

    return render(request, 'login.html', {
        'next': next_url,
        'login_form': form
    })


def logout(request):
    django.contrib.auth.logout(request)
    return redirect('index')
