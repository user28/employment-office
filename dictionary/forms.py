import uuid

from django import forms
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError

from dictionary.models import User

USER_TYPE = (
    ('Candidate', 'Кандидат'),
    ('Employer', 'Работодатель')
)


class RegistrationForm(forms.Form):
    first_name = forms.CharField(max_length=50, required=True, widget=forms.TextInput(attrs={'placeholder': 'Имя'}))
    last_name = forms.CharField(max_length=50, required=True, widget=forms.TextInput(attrs={'placeholder': 'Фамилия'}))
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={'placeholder': 'Электронная почта', 'type': 'email'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': '**************'}, render_value=True))
    user_type = forms.ChoiceField(choices=USER_TYPE, required=True)
    phone = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': 'Телефон'}))

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError("Пользователь с таким Email уже зарегистрирован")
        return email

    def register_user(self):
        form_data = self.cleaned_data
        user = User.objects.create_user(
            uuid.uuid4().hex, form_data['email'], form_data['password'],
            first_name=form_data['first_name'], last_name=form_data['last_name'])

        if user is not None:
            print(form_data['user_type'])
            group = Group.objects.get(name=form_data['user_type'])
            group.user_set.add(user)
            user.save()
        else:
            raise ValidationError("Ошибка регистрации. Повторите попытку позже или свяжитесь с администратором")

        return user


class ResetPasswordForm(forms.Form):
    old_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': '**************'}, render_value=True))
    new_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': '**************'}, render_value=True))
    new_password_confirm = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': '**************'}, render_value=True))

    def clean(self):
        form_data = self.cleaned_data
        if form_data['new_password'] != form_data['new_password_confirm']:
            self._errors["new_password"] = ["Пароли должны совпадать"]  # Will raise a error message
            del form_data['new_password']
        return form_data

    def reset(self, user):
        form_data = self.cleaned_data
        user.set_password(form_data['new_password'])


class LoginForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={'placeholder': 'Электронная почта'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': '**************'}, render_value=True))
    remember_me = forms.BooleanField(required=False)

    def clean_email(self):
        data = self.cleaned_data['email']
        if not User.objects.filter(email=data).exists():
            raise ValidationError('Такая электронная почта не зарегестрирована')
