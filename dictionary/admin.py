from django.contrib import admin

# Register your models here.
from dictionary.models import User, Education, Category, Experience, Age


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category_title', 'category_icon')
    fields = ('category_title', 'category_icon')


admin.site.register(User)
admin.site.register(Education)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Experience)
admin.site.register(Age)
