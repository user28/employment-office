# Generated by Django 2.2 on 2019-04-07 21:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dictionary', '0004_category_category_icon'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='vacancy_categories',
            field=models.ManyToManyField(to='dictionary.Vacancy'),
        ),
    ]
